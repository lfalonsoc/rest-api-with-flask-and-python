# CONTRIBUTING

## How to run the Dockerfile locally

```
IMAGE_NAME
docker build -t name .

Run container
docker run -dp 5005:5000 -w /app -v "$(pwd):/app" IMAGE_NAME "flask run --host 0.0.0.0"
```