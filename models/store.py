from typing import Any

from db import db


class StoreModel(db.Model):
    __tablename__: str = "stores"
    
    id:Any = db.Column(db.Integer, primary_key=True)
    name: Any = db.Column(db.String(80), unique=True, nullable=False)
    tags = db.relationship("TagModel", back_populates="store", lazy="dynamic")
    items = db.relationship(
        "ItemModel",
        back_populates="store",
        lazy="dynamic"
    )
