from typing import Any

from db import db


class UserModel(db.Model):
    __tablename__: str = "users"
    
    id: Any = db.Column(db.Integer, primary_key=True)
    username: Any = db.Column(db.String(80), unique=True, nullable=False)
    password: Any = db.Column(db.String(256), nullable=False)
