from typing import Any

from db import db


class TagModel(db.Model):
    __tablename__: str = "tags"
    
    id: Any = db.Column(db.Integer, primary_key=True)
    name: Any = db.Column(db.String(80), unique=False, nullable=False)
    store_id: Any = db.Column(db.Integer, db.ForeignKey("stores.id"), nullable=False)
    
    store = db.relationship("StoreModel", back_populates="tags")
    items = db.relationship(
        "ItemModel", back_populates="tags", secondary="item_tags"
    )
