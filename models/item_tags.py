from typing import Any

from db import db


class ItemTags(db.Model):
    __tablename__ : str = "item_tags"
    
    id: Any = db.Column(db.Integer, primary_key=True)
    item_id: Any = db.Column(db.Integer, db.ForeignKey("items.id"))
    tag_id: Any = db.Column(db.Integer, db.ForeignKey("tags.id"))
