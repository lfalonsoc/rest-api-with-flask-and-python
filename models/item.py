from typing import Any

from db import db


class ItemModel(db.Model):
    __tablename__: str = "items"
    
    id:Any = db.Column(db.Integer, primary_key=True)
    name: Any = db.Column(db.String(80), unique=True, nullable=False)
    description: Any = db.Column(db.String)
    price: Any = db.Column(
        db.Float(precision=2), unique=False, nullable=False
    )
    store_id: Any = db.Column(
        db.Integer, db.ForeignKey("stores.id"), unique=False, nullable=False
    )
    store = db.relationship("StoreModel", back_populates="items")
    tags = db.relationship("TagModel", back_populates="items", secondary="item_tags")
