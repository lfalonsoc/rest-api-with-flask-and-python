from typing import Any

from marshmallow import Schema, fields


class PlainItemSchema(Schema):
    id: Any = fields.Int(dump_only=True)
    name: Any = fields.Str(required=True)
    price: Any = fields.Float(required=True)


class PlainStoreSchema(Schema):
    id: Any = fields.Int(dump_only=True)
    name: Any = fields.Str()


class PlainTagSchema(Schema):
    id: Any = fields.Int(dump_only=True)
    name: Any = fields.Str()


class ItemUpdateSchema(Schema):
    name: Any = fields.Str()
    price: Any = fields.Float()
    store_id: Any = fields.Int()


class ItemSchema(PlainItemSchema):
    store_id: Any = fields.Int(required=True, load_only=True)
    store: Any = fields.Nested(PlainStoreSchema(), dump_only=True)
    tags: Any = fields.List(fields.Nested(PlainTagSchema()), dump_only=True)


class StoreSchema(PlainStoreSchema):
    items: Any = fields.List(fields.Nested(PlainItemSchema()), dump_only=True)
    tags: Any = fields.List(fields.Nested(PlainTagSchema()), dump_only=True)


class TagSchema(PlainTagSchema):
    store_id: Any = fields.Int(load_only=True)
    store: Any = fields.Nested(PlainStoreSchema(), dump_only=True)
    items: Any = fields.List(fields.Nested(PlainItemSchema()), dump_only=True)


class TagAndItemSchema(Schema):
    message: Any = fields.Str()
    item: Any = fields.Nested(ItemSchema)
    tag: Any = fields.Nested(TagSchema)


class UserSchema(Schema):
    id: Any = fields.Int(dump_only=True)
    username: Any = fields.Str(required=True)
    password: Any = fields.Str(required=True, load_only=True)
