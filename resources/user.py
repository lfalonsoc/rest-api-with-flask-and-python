from typing import Any, Literal

from flask.views import MethodView
from flask_smorest import Blueprint, abort
from passlib.hash import pbkdf2_sha256
from flask_jwt_extended import (
    create_access_token,
    create_refresh_token,
    get_jwt_identity,
    get_jwt,
    jwt_required
)

from db import db
from models import UserModel
from schemas import UserSchema
from blocklist import BLOCKLIST

blp: Blueprint = Blueprint("Users", "users", description="Operation on users")


@blp.route("/register")
class UserRegister(MethodView):
    @blp.arguments(UserSchema)
    def post(
            self, user_data: dict[str, str]
        ) -> tuple[dict[str, str], Literal[201]]:
        if UserModel.query.filter(
            UserModel.username == user_data["username"]
        ).first():
            abort(409, message="A user with that username already exists.")
        
        user: UserModel = UserModel(
            username=user_data["username"],
            password=pbkdf2_sha256.hash(user_data["password"])
        )
        db.session.add(user)
        db.session.commit()
        
        return {"message": "User created successfully."}, 201


@blp.route("/refresh")
class TokenRefresh(MethodView):
    @jwt_required(refresh=True)
    def post(self) -> dict[str, str]:
        current_user: Any = get_jwt_identity()
        new_token: str = create_access_token(identity=current_user, fresh=False)
        jti: Any = get_jwt["jti"]
        BLOCKLIST.add(jti)
        return {"access_token": new_token}


@blp.route("/login")
class UserLogin(MethodView):
    @blp.arguments(UserSchema)
    def post(self, user_data: dict[str, str]) -> dict[str, Any]:
        user: UserModel = UserModel.query.filter(
            UserModel.username == user_data["username"]
        ).first()
        
        if user and pbkdf2_sha256.verify(
            user_data["password"], user.password
        ):
            access_token: str = create_access_token(identity=user.id, fresh=True)
            refresh_token: Any = create_refresh_token(identity=user.id)
            return {"access_token": access_token, "refresh_token": refresh_token}
        
        abort(401, message="Invalid credentials.")


@blp.route("/logout")
class UserLogout(MethodView):
    @jwt_required()
    def post(self) -> dict[str, str]:
        jti: Any = get_jwt()["jti"]
        BLOCKLIST.add(jti)
        return {"message": "Succesffully logged out."}
        


@blp.route("/user/<int:user_id>")
class User(MethodView):
    @blp.response(200, UserSchema)
    def get(self, user_id: int) -> UserModel:
        user: UserModel = UserModel.query.get_or_404(user_id)
        return user
    
    def delete(self, user_id: int) -> tuple[dict[str, str], Literal[200]]:
        user: UserModel = UserModel.query.get_or_404(user_id)
        db.session.delete(user)
        db.session.commit()
        return {"message": "User deleted."}, 200
