from typing import Any, List

from flask.views import MethodView
from flask_smorest import Blueprint, abort
from flask_jwt_extended import jwt_required, get_jwt
from sqlalchemy.exc import SQLAlchemyError

from db import db
from models import ItemModel
from schemas import ItemSchema, ItemUpdateSchema


blp: Blueprint = Blueprint(
    "Items",
    __name__,
    description="Operations on items",
)


@blp.route("/item/<int:item_id>")
class Item(MethodView):
    @blp.response(200, ItemSchema)
    @jwt_required()
    def get(self, item_id: str) -> ItemModel:
        item: ItemModel = ItemModel.query.get_or_404(item_id)
        return item

    @jwt_required()
    def delete(self, item_id: str) -> dict[str, str]:
        jwt: Any = get_jwt()
        if not "is_admin":
            abort(401, message="Admin privilege required.")

        item: ItemModel = ItemModel.query.get_or_404(item_id)
        db.session.delete(item)
        db.session.commit()
        return {"message": "Item deleted."}

    @jwt_required()
    @blp.arguments(ItemUpdateSchema)
    @blp.response(200, ItemSchema)
    def put(self, item_data: dict[str, Any], item_id: str) -> ItemModel:
        item: ItemModel = ItemModel.query.get(item_id)
        if item:
            item.price = item_data["price"]
            item.name = item_data["name"]
        else:
            item: ItemModel = ItemModel(id=item_id, **item_data)

        db.session.add(item)
        db.session.commit()

        return item


@blp.route("/item")
class itemList(MethodView):
    @blp.response(200, ItemSchema(many=True))
    @jwt_required()
    def get(self) -> List[Any]:
        return ItemModel.query.all()
    
    @jwt_required(fresh=True)
    @blp.arguments(ItemSchema)
    @blp.response(201, ItemSchema)
    def post(self, item_data: dict[str, Any]) -> ItemModel:
        item: ItemModel = ItemModel(**item_data)

        try:
            db.session.add(item)
            db.session.commit()
        except SQLAlchemyError:
            abort(500, message="An error ocurred while inserting the item.")

        return item
