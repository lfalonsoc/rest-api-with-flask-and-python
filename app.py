import os
from typing import Any, Literal

from flask import Flask, jsonify
from flask.wrappers import Response
from flask_smorest import Api
from flask_jwt_extended import JWTManager
from flask_migrate import Migrate
from dotenv import load_dotenv

from db import db
import models
from blocklist import BLOCKLIST

from resources.item import blp as ItemBlueprint
from resources.store import blp as StoreBlueprint
from resources.tag import blp as TagBlueprint
from resources.user import blp as UserBlueprint


def create_app(db_url: Any = None) -> Any:
    app: Flask = Flask(__name__)
    load_dotenv()
    
    app.config["API_TITLE"] = "Stores REST API"
    app.config["API_VERSION"] = "v1"
    app.config["OPENAPI_VERSION"] = "3.0.3"
    app.config["OPENAPI_URL_PREFIX"] = "/"
    app.config["OPENAPI_SWAGGER_UI_PATH"] = "/swagger-ui"
    app.config[
        "OPENAPI_SWAGGER_UI_URL"
    ] = "https://cdn.jsdelivr.net/npm/swagger-ui-dist/"
    app.config[
        "SQLALCHEMY_DATABASE_URI"
    ] = db_url or os.getenv("DATABASE_URL", "sqlite:///data.db")
    app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
    app.config["PROPAGATE_EXCEPTIONS"] = True
    db.init_app(app)    
    migrate: Migrate = Migrate(app, db)
    api: Api = Api(app)

    app.config["JWT_SECRET_KEY"] = "luis"
    jwt: JWTManager = JWTManager(app)


    @jwt.token_in_blocklist_loader
    def check_if_token_in_blocklist(
            jwt_header: Any, jwt_payload: Any
        ) -> bool:
        return jwt_payload["jti"] in BLOCKLIST


    @jwt.revoked_token_loader
    def revoke_token_callback(
            jwt_header: Any, jwt_payload: Any
        ) -> tuple[Response, Literal[401]]:
        return (
            jsonify(
                {
                    "description": "The token as been revoked.",
                    "error": "token_revoked",
                }
            ),
            401,
        )


    @jwt.needs_fresh_token_loader
    def token_not_fresh_callback(
        jwt_header: Any, jwt_payload: Any
    ) -> tuple[Response, Literal[401]]:
        return (
            jsonify(
                {
                    "description": "The token is not fresh.",
                    "error": "Fresh_token_required",
                }
            ), 401
        )


    @jwt.additional_claims_loader
    def add_claims_to_jwt(identity) -> dict[str, bool]:
        if identity == 1:
            return {"is_admin": True}
        return {"is_admin": False}


    @jwt.expired_token_loader
    def expired_token_callback(
            jwt_header: Any, jwt_payload: Any
        ) -> tuple[Any, Literal[401]]:
        return (
            jsonify(
                {
                    "message": "Signature verification filed.",
                    "error": "token_expired"
                }
            ),
            401,
        )


    @jwt.invalid_token_loader
    def invalid_token_callback(error: Any) -> tuple[Any, Literal[401]]:
        return (
            jsonify(
                {
                    "message": "Signature verification filed.",
                    "error": "invalid_token"
                }
            ),
            401,
        )


    @jwt.unauthorized_loader
    def missing_token_callback(error: Any) -> tuple[Any, Literal[401]]:
        return (
            jsonify(
                {
                    "description": "Request does not contain an access token.",
                    "error": "authorization_required"
                }
            ),
            401,
        )


    api.register_blueprint(ItemBlueprint)
    api.register_blueprint(StoreBlueprint)
    api.register_blueprint(TagBlueprint)
    api.register_blueprint(UserBlueprint)

    return app
